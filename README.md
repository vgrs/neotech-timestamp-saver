# neotech-timestamp-saver

> ### Deployment instructions

> Create database, database user

 ```mysql
 create database <db>;
 
 CREATE USER '<user>'@'%' IDENTIFIED BY '<password>';
 
 GRANT ALL PRIVILEGES ON *.* TO '<user>'@'%' WITH GRANT OPTION;
 
 create table dates( date_ DATETIME);
 ```

 > Application properties configuration
  ```
  app.db.provider=mysql
  app.db.interruption.timeout=5000 
  app.db.interruption.message=The connection with database was interrupted, trying after %s milliseconds.
  app.db.jdbc=jdbc:mysql://<host>:<port>/<db>
  app.db.jdbc.user=<user>
  app.db.jdbc.password=<password>
  ```
 > Place src/main/resources/config/application.properties to the same directory with jar:
 
 > Compile and run
 ``` 
 mvn clean install package
 
 java -jar timestamp-saver-jar-with-dependencies.jar
 
 java -jar timestamp-saver-jar-with-dependencies.jar -p
 
 ``` 
