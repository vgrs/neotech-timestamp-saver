package net.sf.vgrs.neotech.tstmp.util;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class AppConfigTest {

    AppConfig appConfig;

    @Before
    public void init() throws IOException {
        AppConfig.destroy();
        AppConfig.init(JUnitCostants.APP_PROPERTY_LOCATION);
    }

    @Test
    public void testGetPropertyForDBProvider(){
        Assert.assertNotNull(appConfig.getProperty(AppConfig.PropertyKeys.APP_DB_PROVIDER));
    }

    @Test
    public void testGetPropertyForInterruptionTimeout(){
        Assert.assertNotNull(appConfig.getProperty(AppConfig.PropertyKeys.APP_DB_INTERRUPTION_TIMEOUT));
    }


    @Test
    public void testGetPropertyForInterruptionMessage(){
        Assert.assertNotNull(appConfig.getProperty(AppConfig.PropertyKeys.APP_DB_INTERRUPTION_MESSAGE));
    }

    @Test
    public void testGetPropertyForJDBC(){
        Assert.assertNotNull(appConfig.getProperty(AppConfig.PropertyKeys.APP_DB_JDBC));
        Assert.assertNotNull(appConfig.getProperty(AppConfig.PropertyKeys.APP_DB_JDBC_USER));
        Assert.assertNotNull(appConfig.getProperty(AppConfig.PropertyKeys.APP_DB_JDBC_PASSWORD));
    }
}
