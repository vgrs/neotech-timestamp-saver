package net.sf.vgrs.neotech.tstmp.dao;

import com.mysql.cj.jdbc.exceptions.CommunicationsException;
import net.sf.vgrs.neotech.tstmp.domain.exceptions.DatabaseInterruption;
import net.sf.vgrs.neotech.tstmp.domain.exceptions.UserDefinedException;

import javax.sql.DataSource;
import java.net.ConnectException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TimestampDaoMySqlImpl implements TimestampDao{

    private DataSource dataSource;

    public TimestampDaoMySqlImpl(DataSource dataSource){
        this.dataSource = dataSource;
    }

    @Override
    public void create(Date date) throws UserDefinedException {
        try (Connection connection = dataSource.getConnection()){
            connection.setAutoCommit(false);
            PreparedStatement ps = connection.prepareStatement("insert into dates(date_)values(?)");
            ps.setTimestamp(1, new java.sql.Timestamp(date.getTime()));
            int i = ps.executeUpdate();
            if (i <= 0){
                throw new SQLException("The data not inserted to database. Please try again");
            }
            connection.commit();
        } catch (CommunicationsException | SQLTimeoutException e) {
            e.printStackTrace();
            throw new DatabaseInterruption(e);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new UserDefinedException(e);
        }
    }

    @Override
    public List<Date> read() throws UserDefinedException {
        try (Connection connection = dataSource.getConnection()){
            PreparedStatement ps = connection.prepareStatement("select date_ from dates");
            ResultSet rs = ps.executeQuery();
            List<Date> list = new ArrayList<>();
            while (rs.next()){
                Timestamp timestamp = rs.getTimestamp(1);
                list.add(new Date(timestamp.getTime()));
            }
            return list;
        } catch (SQLTimeoutException e) {
            e.printStackTrace();
            throw new DatabaseInterruption(e);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new UserDefinedException(e);
        }
    }
}
