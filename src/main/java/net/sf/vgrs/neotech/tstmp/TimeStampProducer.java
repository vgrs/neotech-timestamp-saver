package net.sf.vgrs.neotech.tstmp;

import net.sf.vgrs.neotech.tstmp.service.CacheService;

import java.util.Date;

public class TimeStampProducer implements Runnable{

    private CacheService<Date> cacheService;

    public TimeStampProducer(CacheService<Date> cacheService){
        this.cacheService = cacheService;
    }

    @Override
    public void run() {
        while (true){
            try {
                cacheService.put(new Date());
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
