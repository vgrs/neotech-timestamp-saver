package net.sf.vgrs.neotech.tstmp.domain.exceptions;

public class UserDefinedException extends Exception {
    public UserDefinedException() {
    }

    public UserDefinedException(String message) {
        super(message);
    }

    public UserDefinedException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserDefinedException(Throwable cause) {
        super(cause);
    }

    public UserDefinedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

