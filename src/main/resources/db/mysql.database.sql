create database neotech_db;

CREATE USER 'neotech_user'@'%' IDENTIFIED BY 'neotech_pwd';

GRANT ALL PRIVILEGES ON *.* TO 'neotech_user'@'%' WITH GRANT OPTION;

drop table dates;

create table dates( date_ DATETIME);